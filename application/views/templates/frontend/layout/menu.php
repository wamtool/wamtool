<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <!--<a href="<?php echo base_url(); ?>" class="navbar-brand" title="WAM Tool"><b>WAM</b> Tool</a>-->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
        <?php $this->load->view('templates/frontend/layout/navigation');?>
      </div>
    </div>
  </nav>
</header>
