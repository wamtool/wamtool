<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Access_library {

	protected $_table;

	// Constructor
	public function __construct() {
		if (!isset($this->CI)) {
			$this->CI = &get_instance();
		}

		$this->_table = $this->CI->config->item('database_tables');

	}

	// Public methods
	public function is_logged_in() {
		if ($this->aauth->is_loggedin() == true) {
			return true;
		} else {
			return false;
		}
	}

	public function is_admin() {
		if ($this->CI->aauth->is_admin() == true) {
			return true;
		} else {
			return false;
		}
	}

	public function is_user() {
		if ($this->CI->aauth->is_member('User') != true) {
			return false;
		} else {
			return true;
		}
	}

	public function check_logged_in() {
		if ($this->CI->aauth->is_loggedin() != true) {
			redirect('user/login', 'refresh');
			exit();
		}
	}

	public function update_userstatus($form_data, $userid) {
		$this->CI->db->where('id', $userid);
		$this->CI->db->update($this->_table['profile'], $form_data);
		if ($this->CI->db->affected_rows() == '1') {
			return TRUE;
		}
		return FALSE;
	}
}
/* End of file Access_library.php */
/* Location: ./application/libraries/Access_library.php */
