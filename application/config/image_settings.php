<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Image settings
|--------------------------------------------------------------------------
|
| Image names
|
*/

$config['upload_path'] 			= 'uploads';
$config['allowed_types'] 			= 'gif|jpg|png|pdf|doc|txt|xls';
$config['max_size']			= '10000';
$config['remove_spaces'] 				= 'TRUE';
$config['overwrite'] 				= 'FALSE';

/* End of file image_settings.php */
/* Location: ./application/config/image_settings.php */