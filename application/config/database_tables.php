<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
|--------------------------------------------------------------------------
| Database table settings
|--------------------------------------------------------------------------
|
| Table names
|
 */

$config['home'] = 'home';
//$config['navigation'] = 'navigation';

/* End of file database_tables.php */
/* Location: ./application/config/database_tables.php */
