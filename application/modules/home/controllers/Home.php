<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  // Protected or private properties
  protected $_template;

  public function __construct() {
    parent::__construct();
    //$this->load->model('home/home_model','home');
  }

  public function index() {
    $data['homeslider']	= true;
    $data['page_data'] 	= array(
      'title'=> "WAM Tool",
      'description' => "Welcome to WAM Tool.",
    );
    $this->_template['page'] = 'home/home';
    $this->system_library->load($this->_template['page'], $data);
  }
}
