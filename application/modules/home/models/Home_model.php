<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
  // Protected or private properties
  protected $_table;

  // Constructor
  public function __construct() {
    parent::__construct();
    $this->_table = $this->config->item('database_tables');
  }
}
